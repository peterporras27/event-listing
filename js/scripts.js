var marker;
var map;

jQuery(window).load(function() {

    var lat = jQuery('#event-latitude').val();
    var lng = jQuery('#event-longitude').val();

    lat = ( lat == '' ) ? 11.18379077304663: lat;
    lng = ( lng == '' ) ? 122.442626953125: lng;

    var mapOptions = {
        center: new google.maps.LatLng(lat,lng),
        zoom: 8,
        mapTypeControl: true,
        zoomControl: true,
        panControl: true,
        streetViewControl: false,
    };

    map = new google.maps.Map( document.getElementById("map"), mapOptions);
    var point = new google.maps.LatLng(lat, lng);
    marker = new google.maps.Marker({
        position: point,
        map: map,
    });

    google.maps.event.addListener(map, 'click', function(event) {
        var lataddr = event.latLng.lat();
        var longaddr = event.latLng.lng();
        jQuery('#event-latitude').val(lataddr);
        jQuery('#event-longitude').val(longaddr);
        findAddress(event.latLng);
        placeMarker(event.latLng);
    });
});

jQuery(document).ready(function($) {
    jQuery('input[name="event_url"]').blur(function(event) {
        var url = jQuery(this).val();
        if ( isValidURL(url) ) {
            
        } else {
            jQuery(this).val('');
        }
    }); 

    jQuery('input[name="event_date"]').datepicker();
    
});

function placeMarker(location) {
  if ( marker ) {
    marker.setPosition(location);
  } else {
    marker = new google.maps.Marker({
      position: location,
      map: map
    });
  }
}

function findAddress(point) {
    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({latLng: point}, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            var cityindex = results.length-3;
            jQuery('#mapcity').text('( '+results[cityindex].address_components[0].long_name+' )');
            if (results[0]) {
                jQuery('#mapaddr').text('( '+results[0].formatted_address+' )');
            }
        }
    });
}

function isValidURL(userInput) {
    var res = userInput.match(/(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g);
    if(res == null)
        return false;
    else
        return true;
}
<?php 
/*
Plugin Name: CPT Events
Plugin URI: https://bitbucket.org/peterporras27/event-listing
Description: This is a custom post type plugin for events wihch displays a google map for the event location and url of the event.
Author: Peter Jan Michael Porras
Version: 1.0
Author URI: http://peterporras.com/
*/

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

// activate plugin
register_activation_hook( __FILE__, 'cpt_events_activate' );

// deactivate plugin
register_deactivation_hook( __FILE__, 'cpt_events_deactivate' );

// add actions
add_action( 'init', 'cptevents_setup_post_types' );
add_action('admin_init','cpt_events_metaboxes');
add_action('save_post', 'save_post_metadata');
add_action( 'admin_enqueue_scripts', 'load_admin_cpt_events_script' );
add_action( 'admin_enqueue_scripts', 'load_admin_cpt_events_script' );

// activation hook
function cpt_events_activate()
{
    // trigger our function that registers the custom post type
    cptevents_setup_post_types();
 
    // clear the permalinks after the post type has been registered
    flush_rewrite_rules();
}

// deactivate
function cpt_events_deactivate()
{
    // our post type will be automatically removed, so no need to unregister it
    // clear the permalinks to remove our post type's rules
    flush_rewrite_rules();
}

// metabox for custom post type
function cpt_events_metaboxes()
{

	add_meta_box(
		'cpt_events_url_metabox', // id
		'Event External URL', // Title
		'cpt_events_url_metabox', // Function
		'event', // post type
		'normal', // location
		'high', // level
		null
	);

	add_meta_box(
		'cpt_events_date_metabox', // id
		'Event Date', // Title
		'cpt_events_date_metabox', // Function
		'event', // post type
		'normal', // location
		'high', // level
		null
	);

	add_meta_box(
		'cpt_events_map_metabox', // id
		'Event Map Location', // Title
		'cpt_events_map_metabox', // Function
		'event', // post type
		'normal', // location
		'high', // level
		null
	);
}

function load_admin_cpt_events_script( $hook ) 
{
    global $post;
    $scripts = plugins_url( 'js/scripts.js', __FILE__ );
    
    if ( $hook == 'post-new.php' || $hook == 'post.php' ) 
    {
        if ( 'event' === $post->post_type ) 
        {   
            wp_register_style('jquery-ui', '//ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css');
		  	wp_enqueue_style( 'jquery-ui' );   

		    wp_enqueue_script( 
		    	'google-maps', 
		    	'//maps.google.com/maps/api/js?sensor=false'
		    );

		    wp_enqueue_script( 
		    	'cpt-events-scripts', 
		    	$scripts,
		    	array('jquery', 'jquery-ui-core', 'jquery-ui-datepicker'),
		    	time(),
				true
		    );
        }
    }
}

function cpt_events_url_metabox()
{
	global $post;
	$event_url = get_post_meta( $post->ID, 'event_url', true ); ?>
	<input type="text" style="width: 100%" placeholder="http://" name="event_url" value="<?php echo $event_url; ?>" /><br>
	<?php
}


function cpt_events_date_metabox()
{
	global $post;
	$event_date = get_post_meta( $post->ID, 'event_date', true ); ?>
	<input type="text" name="event_date" value="<?php echo $event_date; ?>" /><br>
	<?php
}

function cpt_events_map_metabox()
{
	global $post;
	$event_latitude = get_post_meta( $post->ID, 'event_latitude', true ); 
	$event_longitude = get_post_meta( $post->ID, 'event_longitude', true ); 
	$event_address = get_post_meta( $post->ID, 'event_address', true ); 
	?>
	<label for="event-latitude">
		Latitude: <input type="text" id="event-latitude" name="event_latitude" value="<?php echo $event_latitude; ?>" />
	</label>
	<label for="event-longitude">
		Longitude: <input type="text" id="event-longitude" name="event_longitude" value="<?php echo $event_longitude; ?>" />
	</label>
	<br>
	<p><b>ADDRESS:</b> <span id="mapaddr"><?php echo $event_address; ?></span></p>
	<input type="hidden" name="event_address" value="<?php echo $event_address; ?>">
	<div class="map">
        <p><i>Select a Point in the map to select map coordinates.</i></p>
        <div id="map" style="height: 500px; width: 100%;"></div>
    </div>
	<?php
}

// saving post type metabox data
function save_post_metadata( $post_id ) {

	$metanames = array('event_url','event_date','event_latitude','event_longitude','event_address');

	foreach ($metanames as $metaval) {
		if ( isset($_POST[$metaval]) ){
			update_post_meta(
				$post_id, 
				$metaval, 
				$_POST[$metaval]
			);
		}	
	}
}

// trigger custom post type
function cptevents_setup_post_types()
{
	$labels = array(
		'name'               => _x( 'Events', 'post type general name', 'cpt-events' ),
		'singular_name'      => _x( 'Event', 'post type singular name', 'cpt-events' ),
		'menu_name'          => _x( 'Events', 'admin menu', 'cpt-events' ),
		'name_admin_bar'     => _x( 'Event', 'add new on admin bar', 'cpt-events' ),
		'add_new'            => _x( 'Add New', 'event', 'cpt-events' ),
		'add_new_item'       => __( 'Add New Event', 'cpt-events' ),
		'new_item'           => __( 'New Event', 'cpt-events' ),
		'edit_item'          => __( 'Edit Event', 'cpt-events' ),
		'view_item'          => __( 'View Event', 'cpt-events' ),
		'all_items'          => __( 'All Events', 'cpt-events' ),
		'search_items'       => __( 'Search Events', 'cpt-events' ),
		'parent_item_colon'  => __( 'Parent Events:', 'cpt-events' ),
		'not_found'          => __( 'No event found.', 'cpt-events' ),
		'not_found_in_trash' => __( 'No events found in Trash.', 'cpt-events' )
	);

    // register the "event" custom post type
    register_post_type( 'event', [
    	'labels' => $labels,
    	'public' => true,
    	'label' => 'Events',
    	'description' => __( 'Create custom events with map.', 'cpt-events' ),
    	'exclude_from_search' => false,
    	'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'event' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
    	'supports' => array(
    		'title'
    	),
    	'menu_icon' => plugins_url( 'images/event.png', __FILE__ )
    ]);
}




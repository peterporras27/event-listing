# README #
This is a wordpress plugin which creates a custom post type event with google maps and event link.

### How to install? ###

* [Download Plugin](http://peterporras.com/downloads/cpt-events.zip)
* Go to your plugins page and click "Add New"
* Select upload plugin
* Activate Plugin
* Done!
